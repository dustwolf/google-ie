<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta content="text/html; charset=UTF-8" http-equiv="content-type" /><title>Add Google to IE</title></head>
<body>
<ol>
<li><INPUT TYPE="button" NAME="myButton" VALUE="Googlify!" onClick="window.external.AddSearchProvider('http://google.ctrl-alt-del.si/google.xml')" /></li>
<li><a href="http://www.google.com">Take me home</a>.</li>
</ol>
<p>Download this page's <a href="https://gitlab.com/dustwolf/google-ie">source code</a>. Code is released as Public Domain.</p>
<p>Thanks to all the guys at <a href="http://www.google.com/support/forum/p/Web+Search/thread?tid=6403619863642bf0&hl=en">http://www.google.com/support/forum/p/Web+Search/thread?tid=6403619863642bf0&hl=en</a> for their assistance, the XML file was written based on freely available documentation and their input.</p>
</body>
</html>
